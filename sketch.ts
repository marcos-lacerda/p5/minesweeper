/// <reference path="node_modules/@types/p5/global.d.ts" />

class Cell {
    constructor(
        public col: number,
        public row: number
    ) { }
    revealed: boolean;
    bomb: boolean;
    bombsAround: number;
    active: boolean;
}

const WIDTH = 500;
const HEIGHT = 500;
const CELLS = 30;
const BOMBS = 31;
let board: Cell[][];

function setup() {
    createCanvas(WIDTH + 50, HEIGHT + 50);
    createBoard();
    generateBombs();
    calculateBombsAround();
}

function createBoard() {
    board = [];
    for (var row = 0; row < CELLS; row++) {
        const rowCells: Cell[] = [];
        for (var col = 0; col < CELLS; col++) {
            const cell = new Cell(col, row);
            rowCells.push(cell);
            // cell.revealed = true;
        }
        board.push(rowCells);
    }
}

function mouseClicked(event: MouseEvent) {
    const cell = cellFromPoint(event.x, event.y);
    if (cell) {
        if (cell.bombsAround === 0) {
            revealAll(cell);
        } else {
            cell.revealed = true;
        }
    }
}

function revealAll(cell: Cell) {
    if (cell.revealed) return;
    cell.revealed = true;
    for (let c of getCellsAround(cell)) {
        if (c.bomb) continue;
        if (c.bombsAround === 0) {
            revealAll(c);
        } else {
            c.revealed = true;
        }
    }
}

function mouseMoved(event: MouseEvent) {
    const cell = cellFromPoint(event.x, event.y);
    if (cell) {
        board.forEach(r => r.forEach(c => c.active = false));
        cell.active = true;
        // getCellsAround(cell).forEach(c => c.active = true);
    }
}

function cellFromPoint(x: number, y: number) {
    const bw = WIDTH / CELLS;
    const bh = HEIGHT / CELLS;
    const row = floor((x - 25) / bw);
    const col = floor((y - 25) / bh);
    if ( row >= 0 && row < CELLS &&
         col >= 0 && col < CELLS) {
        return board[row][col];
    }
    return null;
}

function draw() {
    background(255);
    translate(25, 25);
    stroke(100, 23, 230);
    const bw = WIDTH / CELLS;
    const bh = HEIGHT / CELLS;
    textSize(40);
    for (var row = 0; row < CELLS; row++) {
        for (var col = 0; col < CELLS; col++) {
            const x = row * bw;
            const y = col * bh;
            const cell = board[row][col];
            push();
            if (cell.revealed) {
                if (cell.bomb) {
                    text('*', x + bw / 2 - 10, y + bh / 2 + 20);
                } else {
                    if (cell.bombsAround) {
                        text(cell.bombsAround, x + bw / 2 - 12, y + bh / 2 + 15);
                    }
                }
                fill(110, 100);
            } else {
                if (cell.active) {
                    fill(204, 102, 120, 222);
                } else {
                    fill(255,200);
                }
            }
            rect(x, y, bw, bh);
            pop()
        }
    }
}

function generateBombs() {
    let bombsRemaining = BOMBS;
    while (bombsRemaining) {
        const row = floor(random(CELLS));
        const col = floor(random(CELLS));
        const cell = board[row][col];
        if (!cell.bomb) {
            cell.bomb = true;
            bombsRemaining--;
        }
    }
}

function calculateBombsAround() {
    for (var row = 0; row < CELLS; row++) {
        for (var col = 0; col < CELLS; col++) {
            const cell = board[row][col];
            let bombsAround = 0;
            for (let c of getCellsAround(cell)) {
                if (c.bomb) bombsAround++;
            }
            cell.bombsAround = bombsAround;
        }
    }
}

function getCellsAround(cell: Cell): Cell[] {
    const row = cell.row;
    const col = cell.col;
    const ret = [];
    for (var i=row-1; i<=row+1; i++) {
        for (var j=col-1; j<=col+1; j++) {
            if ( i >= 0 && i < CELLS &&
                 j >= 0 && j < CELLS) {
                if ( ! (i == row && j == col) ) {
                    ret.push(board[i][j]);
                }
            }
        }
    }
    return ret;
}

